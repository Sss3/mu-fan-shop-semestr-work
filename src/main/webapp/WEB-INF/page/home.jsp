<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:template title="Main">

    <jsp:attribute name="body">

        <main>
            <div id="slider">
                <img src="<c:url value="/resources/img/slider.png"/>" width="80%" alt=""/>
            </div>


            <div class="main-wrapper">

                <div id="product-list">

                    <span class="title">Новые поступления</span>
                    <a href="#" class="more">Посмотреть все</a>

                    <p class="clear"></p>

                    <div class="product">
                        <span class="discount">25%</span>
                        <div class="product-img">
                            <a href="#"><img src="<c:url value="/resources/img/product.png"/>" alt=""/></a>
                        </div>
                        <p class="product_name">Игровая майка 2014-15</p>
                        <div class="action_block">
                            <div class="price">1500руб</div>
                            <div class="size">S</div>
                            <a href="#" class="basket"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-basket"><g id="shopping-cart"><path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path></g></svg>
                            </a>
                        </div>
                    </div>

                    <div class="product">
                        <span class="discount">25%</span>
                        <div class="product-img">
                            <a href="#"><img src="<c:url value="/resources/img/product.png"/>" alt=""/></a>
                        </div>
                        <p class="product_name">Игровая майка 2014-15</p>
                        <div class="action_block">
                            <div class="price">1500руб</div>
                            <div class="size">S</div>
                            <a href="#" class="basket"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-basket"><g id="shopping-cart"><path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path></g></svg>
                            </a>
                        </div>
                    </div>

                    <div class="product">
                        <span class="discount">25%</span>
                        <div class="product-img">
                            <a href="#"><img src="<c:url value="/resources/img/product.png"/>" alt=""/></a>
                        </div>
                        <p class="product_name">Игровая майка 2014-15</p>
                        <div class="action_block">
                            <div class="price">1500руб</div>
                            <div class="size">S</div>
                            <a href="#" class="basket"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-basket"><g id="shopping-cart"><path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path></g></svg>
                            </a>
                        </div>
                    </div>

                </div>
            </div>




            <div class="banner">
                <img src="<c:url value="/resources/img/welback.png"/>" width="100%"/>
            </div>



            <div class="main-wrapper">

                <div id="product-list">

                    <span class="title">Самые покупаемые</span>
                    <a href="#" class="more">Посмотреть все</a>

                    <p class="clear"></p>

                    <div class="product">
                        <span class="discount">25%</span>
                        <div class="product-img">
                            <a href="#"><img src="<c:url value="/resources/img/product.png"/>" alt=""/></a>
                        </div>
                        <p class="product_name">Игровая майка 2014-15</p>
                        <div class="action_block">
                            <div class="price">1500руб</div>
                            <div class="size">S</div>
                            <a href="#" class="basket"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-basket"><g id="shopping-cart"><path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path></g></svg>
                            </a>
                        </div>
                    </div>

                    <div class="product">
                        <span class="discount">25%</span>
                        <div class="product-img">
                            <a href="#"><img src="<c:url value="/resources/img/product.png"/>" alt=""/></a>
                        </div>
                        <p class="product_name">Игровая майка 2014-15</p>
                        <div class="action_block">
                            <div class="price">1500руб</div>
                            <div class="size">S</div>
                            <a href="#" class="basket"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-basket"><g id="shopping-cart"><path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path></g></svg>
                            </a>
                        </div>
                    </div>

                    <div class="product">
                        <span class="discount">25%</span>
                        <div class="product-img">
                            <a href="#"><img src="<c:url value="/resources/img/product.png"/>" alt=""/></a>
                        </div>
                        <p class="product_name">Игровая майка 2014-15</p>
                        <div class="action_block">
                            <div class="price">1500руб</div>
                            <div class="size">S</div>
                            <a href="#" class="basket"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-basket"><g id="shopping-cart"><path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path></g></svg>
                            </a>
                        </div>
                    </div>

                </div>
            </div>



            <div class="banner">
                <img src="<c:url value="/resources/img/rooney-clevz.png"/>" width="100%"/>
            </div>



            <div class="main-wrapper galery">
                <div class="row">
                    <div class="col-md-3"> <a href="#"> <img src="<c:url value="/resources/img/gallery1.png"/>"/></a></div>
                    <div class="col-md-3"> <a href="#"> <img src="<c:url value="/resources/img/gallery2.png"/>"/></a></div>
                    <div class="col-md-3"> <a href="#"> <img src="<c:url value="/resources/img/gallery3.png"/>"/></a></div>
                    <div class="col-md-3"> <a href="#"> <img src="<c:url value="/resources/img/gallery4.png"/>"/></a></div>
                </div>
                <div class="row">
                    <div class="col-md-3"> <a href="#"> <img src="<c:url value="/resources/img/gallery5.png"/>"/></a></div>
                    <div class="col-md-3"> <a href="#"> <img src="<c:url value="/resources/img/gallery6.png"/>"/></a></div>
                    <div class="col-md-3"> <a href="#"> <img src="<c:url value="/resources/img/gallery7.png"/>"/></a></div>
                    <div class="col-md-3"> <a href="#"> <img src="<c:url value="/resources/img/gallery8.png"/>"/></a></div>
                </div>
            </div>


        </main>

    </jsp:attribute>

</t:template>