<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:template title="Auth">

    <jsp:attribute name="body">
        <main>
        <form action="<c:url value="/auth"/>" method="POST">
            <input type="text" placeholder="Email" name="email" autofocus/>
            <input type="password" name="pass" placeholder="Password"/>

            <button type="submit" class="btn btn-default">Login</button>
        </form>

            </hr>
            <h2>JS</h2>
            <input type="text" placeholder="email" id="email"/>
            <input type="password" placeholder="pass" id="pass"/>
            <button onclick="auth()" >JS GO</button>

        </main>


        <script type="text/javascript">

            function auth() {
                $.post("/jauth", {email: $("#email").val(), password: $("#pass").val()}, endAuth());
            }

            function endAuth() {
                alert(this);
            }

        </script>
    </jsp:attribute>


</t:template>