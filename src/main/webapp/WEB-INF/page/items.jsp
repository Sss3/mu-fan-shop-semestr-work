<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:template title="Items">

    <jsp:attribute name="body">


        <aside>

            <form method="get">

                <div class="category">
                    <a>Категории</a>
                    <ul>
                        <ul class="subcategory">
                            <a href="#">Мужчинам</a>
                            <li><a href="<c:url value="/items/man/form" />">Игровые футболки</a></li>
                            <li><a href="<c:url value="/items/man/shorts" />">Игровые шорты</a></li>
                            <li><a href="<c:url value="/items/man/jacket" />">Куртки/Ветровки</a></li>
                            <li><a href="<c:url value="/items/man/retro" />">Ретро формы</a></li>
                        </ul>
                        <ul class="subcategory">
                            <a href="#">Женщинам</a>
                            <li><a href="<c:url value="/items/women/form" />">Игровые футболки</a></li>
                            <li><a href="<c:url value="/items/women/shorts" />">Игровые шорты</a></li>
                            <li><a href="<c:url value="/items/women/jacket" />">Куртки/Ветровки</a></li>
                            <li><a href="<c:url value="/items/women/retro" />">Ретро формы</a></li>
                        </ul>
                        <ul class="subcategory">
                            <a href="#">Детям</a>
                            <li><a href="<c:url value="/items/child/form" />">Игровые футболки</a></li>
                            <li><a href="<c:url value="/items/child/shorts" />">Игровые шорты</a></li>
                            <li><a href="<c:url value="/items/child/jacket" />">Куртки/Ветровки</a></li>
                            <li><a href="<c:url value="/items/child/retro" />">Ретро формы</a></li>
                        </ul>
                    </ul>

                </div>

                <div class="price">
                    <a href="#">Цена (Руб.)</a>

                    <div class="clear"></div>
                    <div class="from">
                        <span>От:</span>

                        <div class="clear"></div>
                        <input type="number" name="minPrice">
                    </div>
                    <div class="to">
                        <span>До:</span>

                        <div class="clear"></div>
                        <input type="number" name="maxPrice">
                    </div>

                </div>

                <div class="size">
                    <a href="#">Размер</a>

                    <div class="clear"></div>
                    <div class="size-wrap">
                        <div class="check-block">
                            <input type="checkbox" name="sizeS">S
                        </div>
                        <div class="check-block">
                            <input type="checkbox" name="sizeM">M
                        </div>
                        <div class="check-block">
                            <input type="checkbox" name="sizeL">L
                        </div>
                        <div class="check-block">
                            <input type="checkbox" name="sizeXL">XL
                        </div>
                        <div class="check-block">
                            <input type="checkbox" name="sizeXXL">XXL
                        </div>
                        <input type="submit" value="Показать">

                        <div class="clear"></div>
                        <a class="reset" href="<c:url value="/items" />">Сбросить все</a>

                    </div>


                </div>

                <div class="color">

                </div>

            </form>

        </aside>

        <main class="products-page">

            <div id="products-pagination">
                <span>Показаны ${pageSize} результатов из ${allSize}</span>

                <div id="pagi">
                    <span>Товаров на странице:</span>

                    <div class="number" onclick="changePageSize('9')">9</div>
                    <div class="number" onclick="changePageSize('18')">18</div>
                    <div class="number" onclick="changePageSize('0')">Все</div>
                </div>
                <select onchange="changePageSort()">
                    <option value="date" <c:if test="${param.sort == null || param.sort == 'date'}"> selected </c:if> >
                        Сортировать по дате
                    </option>
                    <option value="price" <c:if test="${param.sort == 'price'}"> selected </c:if> >Сортировать по цене
                    </option>
                </select>
            </div>

            <div id="product-list">
                <p class="clear">
                    <c:forEach items="${products}" var="product" varStatus="index">
                <c:if test="${index.count % 3 == 0}"></div>
            <div id="product-list"><p class="clear"></p></c:if>


                <div class="product-wrap">
                    <div class="product">
                        <c:if test="${not empty product.discount && product.discount > 0}"><span
                                class="discount">${product.discount}%</span></c:if>

                        <div class="product-img">
                            <a href="<c:url value="/item/${product.id}" />"><img
                                    src="<c:url value="${product.photo}" />" alt="${product.name}"/></a>
                        </div>
                        <p class="product_name">${product.name}</p>

                        <div class="action_block">
                            <div class="price">${product.price} руб</div>
                            <div class="size">S</div>
                            <a href="#" class="basket">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-basket">
                                    <g id="shopping-cart">
                                        <path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>

                </c:forEach>
            </div>


        </main>


    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script type="text/javascript">

            var minPrice = ${param.minPrice};
            var maxPrice = ${param.maxPrice};
            var sort = ${param.sort};
            var sizeS = ${param.sizeS};
            var sizeM = ${param.sizeM};
            var sizeL = ${param.sizeL};
            var sizeXL = ${param.sizeXL};
            var sizeXXL = ${param.sizeXXL};
            var pageSize = ${param.pageSize};


            function changePageSize(size) {
                alert('test');
                if ((pageSize == null && size == 9) || pageSize == size) return;
                editPage(size, null);
            }

            function changePageSort() {
                alert($(this).val());
                if(sort == null && $(this).val() == 'date') return;
                if(sort == $(this).val()) return;
                editPage(null, $(this).val());
            }

            function editPage(pag, sor) {
                if (sor == null) sor = sort;
                if (pag == null) pag = pageSize;
                location = "<c:url value="${requestScope['javax.servlet.forward.request_uri']}"/>?minPrice=" + minPrice +
                "&maxPrice=" + maxPrice + "&sort=" + sor + "&sizeS=" + sizeS + "&sizeM=" + sizeM + "&sizeL=" + sizeL +
                "&sizeXL=" + sizeXL + "&sizeXXL=" + sizeXXL + "&pageSize=" + pag + "&page=0";
            }

        </script>
    </jsp:attribute>

</t:template>