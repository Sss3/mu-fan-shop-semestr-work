﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Simple Template" pageEncoding="UTF-8" %>
<%@attribute name="title" %>
<%@attribute name="head" fragment="true" %>
<%@attribute name="body" fragment="true" required="true" %>
<%@attribute name="scripts" fragment="true"%>
<!DOCTYPE html>
<html>
<head>
    <script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
    <jsp:include page="/WEB-INF/tags/template/head.jsp"/>
    <jsp:invoke fragment="head"/>
    <title>MU FanShop | ${title}</title>

</head>

<body>
<div class="wrap">
    <jsp:include page="/WEB-INF/tags/template/header.jsp"/>

    <jsp:invoke fragment="body"/>
    <jsp:include page="/WEB-INF/tags/template/modals.jsp"/>
    <jsp:include page="/WEB-INF/tags/template/footer.jsp"/>

</div>
<jsp:include page="/WEB-INF/tags/template/scripts.jsp"/>
<jsp:invoke fragment="scripts"/>
</body>
</html>
