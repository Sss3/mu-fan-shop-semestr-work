<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="../../favicon.ico">
<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<c:url value="/resources/css/styles.css"/>" rel="stylesheet">
<!-- Menu hover -->
<link href="<c:url value="/resources/css/normalize.css"/>" rel="stylesheet">

<script src="<c:url value="/resources/js/modernizr.js"/>"></script> <!-- Modernizr -->