﻿<div class="clear"></div>

<div id="cd-shadow-layer"></div>

<div id="cd-profile">
    <a href="#">Мои заказы</a>
    <a href="#">Отложенные товары</a>
    <a href="#">Мои личные данные</a>
    <a href="#">Мои действия</a>
    <a href="#">Мой баланс</a>
    <a href="#">Обратная связь</a>
    <a href="#">Настройки</a>
    <a href="#">Выход</a>
</div>

<div id="cd-cart">
    <h2>Корзина</h2>
    <ul class="cd-cart-items">
        <li>
            <span class="cd-qty">1x</span> Тест
            <div class="cd-price">Размер: S, 1500 руб.</div>
            <a href="#0" class="cd-item-remove cd-img-replace">Remove</a>
        </li>

    </ul> <!-- cd-cart-items -->

    <div class="cd-cart-total">
        <p>Общая сумма: <span>39.96 руб.</span></p>
    </div> <!-- cd-cart-total -->
    <a class="cd-go-to-cart" href="#0">Перейти к оформлению заказа</a>
</div> <!-- cd-cart -->