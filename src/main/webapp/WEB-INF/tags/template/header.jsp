﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <div class="header-wrapper">
        <header>
            <nav class="navbar">

                <div class="navbar-header logo">
                    <a href="<c:url value="/" />"><img src="<c:url value="/resources/img/logo.png"/>" alt="MU Fan shop"/></a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav nav-justified menu cl-effect-21">
                        <li><a href="<c:url value="/items/man" />">Мужчинам</a></li>
                        <li><a href="<c:url value="/items/women" />">Женщинам</a></li>
                        <li><a href="<c:url value="/items/child" />">Детям</a></li>
                    </ul>
                </div>


                <div class="buttons">
                    <div class="right basket" id="cd-cart-trigger">
                        <a class="cd-img-replace" href="#0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-navbar"><g id="shopping-cart"><path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path></g></svg></a>
                    </div>
                    <div class="right profile" id="cd-profile-trigger">
                        <a class="cd-img-replace" href="#"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-navbar"><g id="person"><path d="M12,12c2.2,0,4-1.8,4-4c0-2.2-1.8-4-4-4C9.8,4,8,5.8,8,8C8,10.2,9.8,12,12,12z M12,14c-2.7,0-8,1.3-8,4v2h16v-2C20,15.3,14.7,14,12,14z"></path></g></svg></a>
                    </div>
                    <div class="right search">
                        <a href="#"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-navbar svg-search"><g id="search"><path d="M15.5,14h-0.8l-0.3-0.3c1-1.1,1.6-2.6,1.6-4.2C16,5.9,13.1,3,9.5,3C5.9,3,3,5.9,3,9.5S5.9,16,9.5,16c1.6,0,3.1-0.6,4.2-1.6l0.3,0.3v0.8l5,5l1.5-1.5L15.5,14z M9.5,14C7,14,5,12,5,9.5S7,5,9.5,5C12,5,14,7,14,9.5S12,14,9.5,14z"></path></g></svg></a>
                    </div>

                </div>


            </nav>
        </header>
    </div>