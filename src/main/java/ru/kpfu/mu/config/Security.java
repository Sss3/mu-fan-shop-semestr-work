package ru.kpfu.mu.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Alexeev Vladimir  06.05.2015
 */
public class Security extends AbstractSecurityWebApplicationInitializer {
}
