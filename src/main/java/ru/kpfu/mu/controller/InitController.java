package ru.kpfu.mu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.mu.model.Category;
import ru.kpfu.mu.model.Role;
import ru.kpfu.mu.model.User;
import ru.kpfu.mu.repository.CategoryRepository;
import ru.kpfu.mu.repository.RoleRepository;
import ru.kpfu.mu.service.UserService;

import java.util.Arrays;

@Controller
@RequestMapping(value = "/init")
public class InitController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody String initMethod() {
        if(roleRepository.findByName("USER") == null) roleRepository.save(new Role("USER"));
        if(roleRepository.findByName("ADMIN") == null) roleRepository.save(new Role("ADMIN"));
        User user = new User();
        user.setEmail("root");
        user.setPass(encoder.encode("root"));
        user.setRoles(Arrays.asList(roleRepository.findByName("USER"), roleRepository.findByName("ADMIN")));
        userService.save(user);

        categoryRepository.save(new Category("man", true));
        categoryRepository.save(new Category("women", true));
        categoryRepository.save(new Category("child", true));
        categoryRepository.save(new Category("form", false));
        categoryRepository.save(new Category("shorts", false));
        categoryRepository.save(new Category("jacket", false));
        categoryRepository.save(new Category("retro", false));

        return "INIT";
    }

}
