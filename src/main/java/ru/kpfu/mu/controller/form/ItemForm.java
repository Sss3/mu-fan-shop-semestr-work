package ru.kpfu.mu.controller.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.mu.model.Category;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.model.ItemSize;
import ru.kpfu.mu.model.Size;
import ru.kpfu.mu.service.ItemService;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

public class ItemForm {

    @Autowired
    private ItemService itemService;

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private MultipartFile photo;

    @NotNull
    @Min(0)
    private Float price;

    private Integer discount;

    private Integer sizeS;

    private Integer sizeM;

    private Integer sizeL;

    private Integer sizeXL;

    private Integer sizeXXL;

    private Long mainCategory;

    private Long subCategory;

    public ItemForm() {

    }

    public ItemForm(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.price = item.getPrice();
        this.discount = item.getDiscount();
        this.sizeS = item.getItemSize(Size.S).getCount();
        this.sizeM = item.getItemSize(Size.M).getCount();
        this.sizeL = item.getItemSize(Size.L).getCount();
        this.sizeXL = item.getItemSize(Size.XL).getCount();
        this.sizeXXL = item.getItemSize(Size.XXL).getCount();
        this.mainCategory = item.getMainCategory().getId();
        this.subCategory = item.getSubCategory().getId();
    }

    public Item getInstance() {
        Item item = (id != null) ? itemService.getById(id) : new Item();
        item.setName(name);
        item.setPrice(price);
        item.setDiscount(discount);
        item.setMainCategory(new Category(mainCategory));
        item.setSubCategory(new Category(subCategory));
        itemSizes(item);
        return item;
    }

    private void itemSizes(Item item) {
        Set<ItemSize> set = (item.getItemSizes() != null) ? item.getItemSizes() : new HashSet<>();
        if(set.isEmpty()) {
            set.add(new ItemSize(item, sizeS, Size.S));
            set.add(new ItemSize(item, sizeM, Size.M));
            set.add(new ItemSize(item, sizeL, Size.L));
            set.add(new ItemSize(item, sizeXL, Size.XL));
            set.add(new ItemSize(item, sizeXXL, Size.XXL));
            item.setItemSizes(set);
        } else {
            item.getItemSize(Size.S).setCount(sizeS);
            item.getItemSize(Size.M).setCount(sizeM);
            item.getItemSize(Size.L).setCount(sizeL);
            item.getItemSize(Size.XL).setCount(sizeXL);
            item.getItemSize(Size.XXL).setCount(sizeXXL);
        }

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        if(discount == null || discount < 0) {
            this.discount = 0;
            return;
        }
        this.discount = discount;
    }

    public Integer getSizeS() {
        return sizeS;
    }

    public void setSizeS(Integer sizeS) {
        if(sizeS == null || sizeS < 0) {
            this.sizeS = 0;
            return;
        }
        this.sizeS = sizeS;
    }

    public Integer getSizeM() {
        return sizeM;
    }

    public void setSizeM(Integer sizeM) {
        if(sizeM == null || sizeM < 0) {
            this.sizeM = 0;
            return;
        }
        this.sizeM = sizeM;
    }

    public Integer getSizeL() {
        return sizeL;
    }

    public void setSizeL(Integer sizeL) {
        if(sizeL == null || sizeL < 0) {
            this.sizeL = 0;
            return;
        }
        this.sizeL = sizeL;
    }

    public Integer getSizeXL() {
        return sizeXL;
    }

    public void setSizeXL(Integer sizeXL) {
        if(sizeXL == null || sizeXL < 0) {
            this.sizeXL = 0;
            return;
        }
        this.sizeXL = sizeXL;
    }

    public Integer getSizeXXL() {
        return sizeXXL;
    }

    public void setSizeXXL(Integer sizeXXL) {
        if(sizeXXL == null || sizeXXL < 0) {
            this.sizeXXL = 0;
        }
        this.sizeXXL = sizeXXL;
    }

    public Long getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(Long mainCategory) {
        this.mainCategory = mainCategory;
    }

    public Long getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(Long subCategory) {
        this.subCategory = subCategory;
    }
}
