package ru.kpfu.mu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.service.ItemService;

@Controller
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String itemsPageWithOutMainAndSubCategory(@RequestParam(required = false) Float minPrice,
                                                     @RequestParam(required = false) Float maxPrice,
                                                     @RequestParam(required = false) Integer pageSize,
                                                     @RequestParam(required = false) String sort,
                                                     @RequestParam(required = false) Boolean sizeS,
                                                     @RequestParam(required = false) Boolean sizeM,
                                                     @RequestParam(required = false) Boolean sizeL,
                                                     @RequestParam(required = false) Boolean sizeXL,
                                                     @RequestParam(required = false) Boolean sizeXXL,
                                                     @RequestParam(required = false) Integer page,
                                                     ModelMap map) {
        return itemsPage(null, null, minPrice, maxPrice, pageSize, sort, sizeS, sizeM, sizeL, sizeXL, sizeXXL, page, map);
    }

    @RequestMapping(value = "/items/{mainCategory}", method = RequestMethod.GET)
    public String itemsPageWithOutSubCategory(@PathVariable("mainCategory") String category,
                                                     @RequestParam(required = false) Float minPrice,
                                                     @RequestParam(required = false) Float maxPrice,
                                                     @RequestParam(required = false) Integer pageSize,
                                                     @RequestParam(required = false) String sort,
                                                     @RequestParam(required = false) Boolean sizeS,
                                                     @RequestParam(required = false) Boolean sizeM,
                                                     @RequestParam(required = false) Boolean sizeL,
                                                     @RequestParam(required = false) Boolean sizeXL,
                                                     @RequestParam(required = false) Boolean sizeXXL,
                                                     @RequestParam(required = false) Integer page,
                                                     ModelMap map) {
        return itemsPage(category, null, minPrice, maxPrice, pageSize, sort, sizeS, sizeM, sizeL, sizeXL, sizeXXL, page, map);
    }


    @RequestMapping(value = "/items/{mainCategory}/{subCategory}", method = RequestMethod.GET)
    public String itemsPage(@PathVariable("mainCategory") String category,
                            @PathVariable("subCategory")String subcategory,
                            @RequestParam(required = false) Float minPrice,
                            @RequestParam(required = false) Float maxPrice,
                            @RequestParam(required = false) Integer pageSize,
                            @RequestParam(required = false) String sort,
                            @RequestParam(required = false) Boolean sizeS,
                            @RequestParam(required = false) Boolean sizeM,
                            @RequestParam(required = false) Boolean sizeL,
                            @RequestParam(required = false) Boolean sizeXL,
                            @RequestParam(required = false) Boolean sizeXXL,
                            @RequestParam(required = false) Integer page,
                            ModelMap map) {

        Page<Item> items = itemService.getPageItems(category, subcategory, minPrice, maxPrice, pageSize, sort, sizeS, sizeM, sizeL, sizeXL, sizeXXL, page);
        map.put("products", items.getContent());
        map.put("pageSize", (items.getSize() < items.getTotalPages())? items.getSize() : items.getTotalElements());
        map.put("allSize", items.getTotalElements());
        return "items";
    }

}
