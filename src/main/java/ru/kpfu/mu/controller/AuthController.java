package ru.kpfu.mu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.mu.auth.AuthService;
import ru.kpfu.mu.auth.AuthUser;
import ru.kpfu.mu.util.AuthUtil;

@Controller
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private PasswordEncoder encoder;


    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    public String renderAuthPage() {
        if(AuthUtil.isAuthenticated()) return "redirect:/";
        return "auth";
    }

    @RequestMapping(value = "/jauth", method = RequestMethod.POST)
    public @ResponseBody String auth(@RequestParam String email, @RequestParam String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(email, password);
        AuthUser user = (AuthUser) authService.loadUserByUsername(email);
        token.setDetails(user);
        if(user != null && encoder.matches(password, user.getPassword())) {
            AuthUtil.setAuthentication(user);
            return "ok";
        } else {
            return "bad";
        }
    }

}
