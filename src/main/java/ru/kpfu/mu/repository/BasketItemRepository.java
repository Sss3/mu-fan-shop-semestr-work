package ru.kpfu.mu.repository;

import org.springframework.data.repository.CrudRepository;
import ru.kpfu.mu.model.BasketItem;

public interface BasketItemRepository extends CrudRepository<BasketItem, Long> {
}
