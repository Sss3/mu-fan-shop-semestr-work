package ru.kpfu.mu.auth;

import org.hibernate.Hibernate;
import org.springframework.data.jpa.repository.utils.JpaClassUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.kpfu.mu.model.Role;
import ru.kpfu.mu.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Alexeev Vladimir  06.05.2015
 */
public class AuthUser implements UserDetails {

    private static final long serialVersionUID = 8574487612161942912L;
    private User user;

    private AuthUser(){}

    public AuthUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> roles = new ArrayList<>();
        user.getRoles().forEach((role) -> roles.add(new SimpleGrantedAuthority(role.getName())));
        return roles;
    }

    @Override
    public String getPassword() {
        return user.getPass();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
