package ru.kpfu.mu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Category implements Serializable{

    private static final long serialVersionUID = -15953079497441083L;
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;
    private Boolean main;

    public Category() {}

    public Category(Long id) {
        this.id = id;
    }

    public Category(String name, Boolean main) {
        this.name = name;
        this.main = main;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getMain() {
        return main != null && main;
    }

    public void setMain(Boolean main) {
        this.main = main;
    }
}
