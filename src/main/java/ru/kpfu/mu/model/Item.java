package ru.kpfu.mu.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

@Entity
public class Item implements Serializable{

    private static final long serialVersionUID = -8727326277750033108L;
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String photo;

    @Column(nullable = false)
    private Float price;

    private Integer discount;

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY)
    private Set<ItemSize> itemSizes;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Category mainCategory;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Category subCategory;

    public Item() {

    }

    public Item(Long id) {
        this.id = id;
    }

    public Item(String name, String photo, Float price, Category mainCategory, Category subCategory) {
        this.name = name;
        this.photo = photo;
        this.price = price;
        this.mainCategory = mainCategory;
        this.subCategory = subCategory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getDiscount() {
        if(discount == null) return 0;
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Set<ItemSize> getItemSizes() {
        return itemSizes;
    }

    public ItemSize getItemSize(Size size) {
        for (ItemSize itemSize : itemSizes) {
            if(itemSize.getSize().equals(size)) return itemSize;
        }
        return null;
    }

    public void setItemSizes(Set<ItemSize> itemSizes) {
        this.itemSizes = itemSizes;
    }

    public Category getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(Category mainCategory) {
        this.mainCategory = mainCategory;
    }

    public Category getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(Category subCategory) {
        this.subCategory = subCategory;
    }
}
