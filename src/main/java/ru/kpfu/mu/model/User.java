package ru.kpfu.mu.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Alexeev Vladimir  06.05.2015
 */
@Entity
@Table(name = "users")
public class User implements Serializable{

    private static final long serialVersionUID = -3242007294567433059L;
    @Id
    @GeneratedValue
    private Long id;

    private String email;
    private String pass;

    @ManyToMany(fetch = FetchType.EAGER)
//    @ManyToMany
    @JoinTable
    private List<Role> role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public List<Role> getRoles() {
        return role;
    }

    public void setRoles(List<Role> roles) {
        this.role = roles;
    }

}
