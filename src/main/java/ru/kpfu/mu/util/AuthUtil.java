package ru.kpfu.mu.util;

import org.jetbrains.annotations.Nullable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.kpfu.mu.auth.AuthUser;
import ru.kpfu.mu.model.User;

import java.util.Arrays;

public class AuthUtil {

    public static @Nullable User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return isNoAuth(authentication) ? null : (User) authentication.getPrincipal();
    }

    public static @Nullable Long getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return isNoAuth(authentication) ? null : ((User) authentication.getPrincipal()).getId();
    }

    private static boolean isNoAuth(Authentication authentication) {
        return authentication == null || !(authentication instanceof UsernamePasswordAuthenticationToken);
    }

    public static boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !isNoAuth(authentication);
    }

    public static Authentication setAuthentication(AuthUser user) {
        Authentication authToken =
                new UsernamePasswordAuthenticationToken(
                        user,
                        null,
                        user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authToken);
        return authToken;
    }

}
