package ru.kpfu.mu.service;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.model.ItemSize;

import java.util.List;
import java.util.Set;

public interface ItemService {

    Page<Item> getPageItems(String category, String subcategory, Float minPrice, Float maxPrice,
                               Integer pageSize, String sort, Boolean sizeS, Boolean sizeM,
                               Boolean sizeL, Boolean sizeXL, Boolean sizeXXL, Integer page);

    Item getById(Long id);

    Set<ItemSize> save(Set<ItemSize> itemSizes);

    Item save(Item item);

}
