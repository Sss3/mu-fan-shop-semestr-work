package ru.kpfu.mu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kpfu.mu.model.Category;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.model.ItemSize;
import ru.kpfu.mu.model.Size;
import ru.kpfu.mu.repository.CategoryRepository;
import ru.kpfu.mu.repository.ItemRepository;
import ru.kpfu.mu.repository.ItemSizeRepository;
import ru.kpfu.mu.service.ItemService;

import java.util.*;

@Service
public class ItemServiceImpl implements ItemService {

    private static final String SORT_BY_DATE = "date";
    private static final String SORT_BY_PRICE = "price";

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemSizeRepository itemSizeRepository;

    @Override
    public Page<Item> getPageItems(String category, String subcategory, Float minPrice, Float maxPrice,
                                     Integer pageSize, String sort, Boolean sizeS, Boolean sizeM,
                                     Boolean sizeL, Boolean sizeXL, Boolean sizeXXL, Integer page) {

        if(minPrice == null) minPrice = 0F;
        if(maxPrice == null) maxPrice = Float.MAX_VALUE;
        if(pageSize == null) pageSize = 9;
        if(pageSize.equals(0)) pageSize = Integer.MAX_VALUE;
        if(sort == null || sort.isEmpty()) sort = SORT_BY_DATE;
        if(sort != null && !sort.equals(SORT_BY_DATE) && !sort.equals(SORT_BY_PRICE)) sort = SORT_BY_DATE;
        if(sizeS == null) sizeS = true;
        if(sizeM == null) sizeM = true;
        if(sizeL == null) sizeL = true;
        if(sizeXL == null) sizeXL = true;
        if(sizeXXL == null) sizeXXL = true;
        if(page == null) page = 0;


        Category mainCategory = categoryRepository.findByName(category);
        if(mainCategory == null || category.isEmpty()) return itemRepository.findAll(new PageRequest(page, pageSize));
        Category subCategory = categoryRepository.findByName(subcategory);

        Collection<Size> sizes = getSizes(sizeS, sizeM, sizeL, sizeXL, sizeXXL);
        Sort sorting = (sort.equals(SORT_BY_DATE)) ? new Sort(new Sort.Order(Sort.Direction.DESC, "id"))
                : new Sort(new Sort.Order(Sort.Direction.ASC, "price"));

        if(subCategory == null || subcategory.isEmpty()) return itemRepository
                .findToPage(mainCategory, minPrice, maxPrice, sizes, new PageRequest(page, pageSize, sorting));

        return itemRepository.findToPage(mainCategory, subCategory, minPrice, maxPrice, sizes, new PageRequest(page, pageSize, sorting));
    }

    @Override
    public Item getById(Long id) {
        return itemRepository.findOne(id);
    }

    @Override
    public Set<ItemSize> save(Set<ItemSize> itemSizes) {
        Set<ItemSize> set = new HashSet<>();
        itemSizeRepository.save(itemSizes).forEach((itemSize) -> set.add(itemSize));
        return set;
    }

    @Override
    public Item save(Item item) {
        return itemRepository.save(item);
    }

    private Collection<Size> getSizes(Boolean sizeS, Boolean sizeM, Boolean sizeL, Boolean sizeXL, Boolean sizeXXL) {
        Collection<Size> sizes = new ArrayList<>(6);
        if(sizeS) sizes.add(Size.S);
        if(sizeM) sizes.add(Size.M);
        if(sizeL) sizes.add(Size.L);
        if(sizeXL) sizes.add(Size.XL);
        if(sizeXXL) sizes.add(Size.XXL);
        return sizes;
    }

}
